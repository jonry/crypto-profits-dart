# Crypto Profits Dart
WARNING: USE AT YOUR OWN RISK! WE TAKE NO RESPONSIBILITY FOR MISCALCULATIONS!
Compute profits for given transaction history written in Dart.

Select a CSV File containing the transaction history, a supported marketplace and the year you want to compute the taxes for.
If you want to compute all time profits set the year to 0.
Timespan until tax free is set to 1 year.

# Build and Run
Run `dart compile exe ./bin/crypto_profits.dart`. This will create an executable within the bin directory. Then run `./bin/crypto_profits.exe <year> <path-to-tradehistory.csv> <marketplace> (<path-to-tradehistory.csv> <marketplace>)*` 