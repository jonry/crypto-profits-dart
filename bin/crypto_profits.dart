import 'package:crypto_profits/crypto_profits.dart';
import 'package:crypto_profits/models/marketplace.dart';
import 'package:crypto_profits/models/trade_history.dart';

Future<void> main(List<String> arguments) async {
  if (arguments.length % 2 == 0) {
    throw Exception('not enough arguments!');
  } else if (arguments[1] == 'help') {
    print(
        'Usage: provide the year of interest and as many pairs of path-to-tradehistory and marketplace as you like');
    print('<program> <year> (<path to trade history> <marketplace>)*');
    print(
        'e.g. ./crypto_profits 2020 data/tradehistory1.csv bitpanda data/tradehistory2.csv bitpanda-pro');
    print('supported marketplaces atm: bitpanda, bitpanda-pro');
    return;
  }

  var history = <TradeHistory>[];

  final year = int.parse(arguments[0]);

  for (var i = 1; i < arguments.length; i += 2) {
    var taHistory = TradeHistory(
        path: arguments[i], marketplace: _parseMarketplace(arguments[i + 1]));

    history.add(taHistory);
  }

  var profit = await CryptoProfits.computeTradeProfits(
      history, year, Duration(days: 365));

  print("Profits: $profit");
}

MarketPlace _parseMarketplace(String marketplace) {
  switch (marketplace) {
    case 'bitpanda':
      return MarketPlace.bitpanda;
    case 'bitpanda-pro':
      return MarketPlace.bitpanaPro;
    default:
      throw Exception('could not parse marketplace');
  }
}
