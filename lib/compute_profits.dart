import 'package:crypto_profits/models/transaction.dart';
import 'package:crypto_profits/models/transaction_type.dart';

class ComputeProfits {
  static double computeProfits(
      List<Transaction> transactions, int year, Duration timespanUntilTaxFree) {
    var buyTransactions = transactions
        .where((element) => element.transactionType == TransactionType.buy)
        .toList();

    var sellTransactions = transactions
        .where((element) => element.transactionType == TransactionType.sell)
        .toList();

    var profit = .0;

    for (var sell in sellTransactions) {
      var buysToRemove = <int>[];
      for (var i = 0; i < buyTransactions.length; i++) {
        var buy = buyTransactions[i];
        if (sell.asset == buy.asset) {
          var sellDifference = buy.amountAsset - sell.amountAsset;

          if (sellDifference > .0) {
            // more bought than sold
            // get profit of entire sell

            var assetsSold = sell.amountAsset;

            var tmpProfit = sell.amountAsset * sell.assetMarketPrice -
                assetsSold * buy.assetMarketPrice;

            if (_timestampsFitCriteria(
                buy.timestamp, sell.timestamp, timespanUntilTaxFree, year)) {
              profit += tmpProfit;
            }

            buy.amountAsset -= assetsSold;

            break;
          } else if (sellDifference < .0) {
            // more sold than bought
            // get profit for the amount bought

            var assetsSold = buy.amountAsset;

            var tmpProfit = assetsSold * sell.assetMarketPrice -
                buy.amountAsset * buy.assetMarketPrice;

            if (_timestampsFitCriteria(
                buy.timestamp, sell.timestamp, timespanUntilTaxFree, year)) {
              profit += tmpProfit;
            }

            sell.amountAsset -= assetsSold;

            buysToRemove.add(i);
          } else {
            // as much sold as bought
            var tmpProfit = sell.amountAsset * sell.assetMarketPrice -
                buy.amountAsset * buy.assetMarketPrice;

            if (_timestampsFitCriteria(
                buy.timestamp, sell.timestamp, timespanUntilTaxFree, year)) {
              profit += tmpProfit;
            }

            buysToRemove.add(i);
          }
        }
      }

      for (var index in buysToRemove) {
        buyTransactions.removeAt(index);
      }
    }

    return profit;
  }

  static bool _timestampsFitCriteria(
      DateTime buy, DateTime sell, Duration timespanUntilTaxFree, int year) {
    var diff = sell.difference(buy);
    // has profit to be taxed according to timespan
    if (sell.difference(buy) < timespanUntilTaxFree ||
        year == 0) // year == 0 -> enable all time profit calculation
    {
      // was the asset sold in the selected year
      // if year is 0 it gets added
      if (sell.year == year || year == 0) {
        return true;
      }
    }
    return false;
  }
}
