import 'package:crypto_profits/compute_profits.dart';
import 'package:crypto_profits/data_provider.dart';
import 'package:crypto_profits/models/trade_history.dart';
import 'package:crypto_profits/models/transaction.dart';

class CryptoProfits {
  static Future<double> computeTradeProfits(List<TradeHistory> tradeHistory,
      int year, Duration timespanUntilTaxFree) async {
    var trades = <Transaction>[];

    for (var history in tradeHistory) {
      var tas = await DataProvider.readCSV(history.path, history.marketplace);

      for (var ta in tas) {
        trades.add(ta);
      }
    }

    trades.sort((a, b) => a.timestamp.compareTo(b.timestamp));

    var profits =
        ComputeProfits.computeProfits(trades, year, timespanUntilTaxFree);

    return profits;
  }
}
