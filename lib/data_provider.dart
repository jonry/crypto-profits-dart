import 'dart:io';

import 'package:crypto_profits/models/asset.dart';
import 'package:crypto_profits/models/marketplace.dart';
import 'package:crypto_profits/models/transaction_type.dart';
import 'package:csv/csv.dart';
import 'package:crypto_profits/models/transaction.dart';

class DataProvider {
  static Future<List<Transaction>> readCSV(
      String filePath, MarketPlace marketPlace) async {
    final fileRef = File(filePath);
    final fileContent = await fileRef.readAsString();
    var csvTable = const CsvToListConverter().convert(fileContent);
    // remove csv header
    csvTable.removeAt(0);

    switch (marketPlace) {
      case MarketPlace.binance:
        {
          return _parseBinance(csvTable);
        }
      case MarketPlace.bitpanda:
        {
          return _parseBitpanda(csvTable);
        }
      case MarketPlace.bitpanaPro:
        {
          return _parseBitpandaPro(csvTable);
        }
    }
  }

  static List<Transaction> _parseBinance(List<List<dynamic>> csvTable) {
    throw Exception('not implmented');
  }

  static List<Transaction> _parseBitpanda(List<List<dynamic>> csvTable) {
    var result = <Transaction>[];
    for (var row in csvTable) {
      final uuid = row[0].toString();
      final timestamp = DateTime.parse(row[1].toString());
      final transactionType = _parseTransactionType(row[2].toString());

      var amountAsset = double.tryParse(row[6].toString());
      if (amountAsset == null && row[6] == '-') {
        amountAsset = 0;
      } else if (amountAsset == null) {
        throw Exception('could not parse amount asset - got ${row[6]}');
      }

      final asset = _parseAsset(row[7].toString());

      var assetMarketPrice = double.tryParse(row[8].toString());
      if (assetMarketPrice == null && row[8] == '-') {
        assetMarketPrice = 0;
      } else if (assetMarketPrice == null) {
        throw Exception('could not parse asset market price - got ${row[8]}');
      }

      result.add(Transaction(
        uuid: uuid,
        timestamp: timestamp,
        transactionType: transactionType,
        amountAsset: amountAsset,
        asset: asset,
        assetMarketPrice: assetMarketPrice,
      ));
    }

    return result;
  }

  static List<Transaction> _parseBitpandaPro(List<List<dynamic>> csvTable) {
    var result = <Transaction>[];
    for (var row in csvTable) {
      final uuid = row[1].toString();
      final timestamp = DateTime.parse(row[10].toString());
      final transactionType = _parseTransactionType(row[2].toString());

      var amountAsset = double.tryParse(row[4].toString());
      if (amountAsset == null && row[4] == '-') {
        amountAsset = 0;
      } else if (amountAsset == null) {
        throw Exception('could not parse amount asset - got ${row[4]}');
      }

      final asset = _parseAsset(row[5].toString());

      var assetMarketPrice = double.tryParse(row[6].toString());
      if (assetMarketPrice == null && row[6] == '-') {
        assetMarketPrice = 0;
      } else if (assetMarketPrice == null) {
        throw Exception('could not parse asset market price - got ${row[6]}');
      }

      result.add(Transaction(
        uuid: uuid,
        timestamp: timestamp,
        transactionType: transactionType,
        amountAsset: amountAsset,
        asset: asset,
        assetMarketPrice: assetMarketPrice,
      ));
    }
    return result;
  }

  static TransactionType _parseTransactionType(String type) {
    switch (type) {
      case 'buy':
      case 'BUY':
        {
          return TransactionType.buy;
        }
      case 'sell':
      case 'SELL':
        {
          return TransactionType.sell;
        }
      case 'deposit':
        {
          return TransactionType.deposit;
        }
      case 'withdrawal':
        {
          return TransactionType.withdrawal;
        }
      case 'transfer':
        {
          return TransactionType.transfer;
        }
      default:
        {
          throw Exception('could not parse transaction type $type');
        }
    }
  }

  static Asset _parseAsset(String asset) {
    switch (asset) {
      case 'EUR':
        {
          return Asset.eur;
        }
      case 'USD':
        {
          return Asset.usd;
        }
      case 'BTC':
        {
          return Asset.btc;
        }

      case 'ETH':
        {
          return Asset.eth;
        }

      case 'ADA':
        {
          return Asset.ada;
        }

      case 'LINK':
        {
          return Asset.link;
        }

      case 'DOT':
        {
          return Asset.dot;
        }

      case 'VET':
        {
          return Asset.vet;
        }

      case 'XTZ':
        {
          return Asset.xtz;
        }

      case 'BEST':
        {
          return Asset.best;
        }
      default:
        {
          throw Exception('could not parse asset $asset');
        }
    }
  }
}
