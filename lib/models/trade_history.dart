import 'package:crypto_profits/models/marketplace.dart';

class TradeHistory {
  final String path;
  final MarketPlace marketplace;

  TradeHistory({
    required this.path,
    required this.marketplace,
  });
}
