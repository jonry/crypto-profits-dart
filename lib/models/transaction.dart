import 'package:crypto_profits/models/asset.dart';
import 'package:crypto_profits/models/transaction_type.dart';

class Transaction {
  final String uuid;
  final DateTime timestamp;
  final TransactionType transactionType;
  final Asset asset;
  double amountAsset;
  final double assetMarketPrice;

  Transaction({
    required this.uuid,
    required this.timestamp,
    required this.transactionType,
    required this.asset,
    required this.amountAsset,
    required this.assetMarketPrice,
  });

  DateTime get ts {
    return timestamp;
  }

  @override
  String toString() {
    return '${uuid.toString()},${timestamp.toString()},${transactionType.toString()},$amountAsset,${asset.toString()},$assetMarketPrice';
  }
}
